
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import io.restassured.path.json.JsonPath;
import io.restassured.path.json.exception.JsonPathException;

public class Test {
	String url;
	String command[] = new String[10];
	String key;
	int status[] = new int[10];
	String var1[] = new String[10];
	String var2[] = new String[10];
	String var3[] = new String[10];
	String var4[] = new String[10];
	String var5[] = new String[10];
	String var6[] = new String[10];
	int var7[] = new int[10];
	int var8[] = new int[10];
	int rowStart, rowEnd;

	@BeforeTest
	public void test() throws IOException, NoSuchAlgorithmException {
		Excel read = new Excel();
		rowStart = read.getRowStart();
		rowEnd = read.getRowEnd();
		url = read.getUrl();
		key = read.getKey();
		command = read.getCommand();
		var1 = read.getVar(1);
		var2 = read.getVar(2);
		var3 = read.getVar(3);
		var4 = read.getVar(4);
		var5 = read.getVar(5);
		var6 = read.getVar(6);
		var7 = read.getIntVar(7);
		var8 = read.getIntVar(8);
		status = read.getStatus();

	}

	@org.testng.annotations.Test(priority = 0, description = "Verify whether card is saved or not")
	void verify_Save_card() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[1], key, var1[1], var2[1], var3[1], var4[1], var5[1], var6[1], var7[1],
				var8[1]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[1], actual);

	}

	@org.testng.annotations.Test(priority = 1, description = "Verify whether card is fetched or not")
	void verify_get_card() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[2], key, var1[2], var2[2], var3[2], var4[2], var5[2], var6[2], var7[2],
				var8[2]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[2], actual);

	}

	@org.testng.annotations.Test(priority = 2, description = "Verify whether card is deleted or not")
	void verify_delete_card() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[3], key, var1[3], var2[3], var3[3], var4[3], var5[3], var6[3], var7[3],
				var8[3]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[3], actual);

	}

	@org.testng.annotations.Test(priority = 3, description = "Verify whether card can be fetched after deleting")
	void get_user_card_that_not_exist() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[4], key, var1[4], var2[4], var3[4], var4[4], var5[4], var6[4], var7[4],
				var8[4]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[4], actual);

	}

	@org.testng.annotations.Test(priority = 4, description = "Verify whether card can be deleted again")
	void delete_user_card_that_not_exist() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[5], key, var1[5], var2[5], var3[5], var4[5], var5[5], var6[5], var7[5],
				var8[5]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[5], actual);

	}

	@org.testng.annotations.Test(priority = 5, description = "Verify whether card with wrong details can be saved or not")
	void save_user_card_with_wrong_details() throws NoSuchAlgorithmException, JsonPathException {

		Pair res = decide.decide1(url, command[6], key, var1[6], var2[6], var3[6], var4[6], var5[6], var6[6], var7[6],
				var8[6]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[6], actual);

	}

	@org.testng.annotations.Test(priority = 6)
	void verifyHeader() throws NoSuchAlgorithmException {

		for (int i = rowStart + 1; i <= rowEnd; ++i) {
			Pair res = decide.decide1(url, command[i], key, var1[i], var2[i], var3[i], var4[i], var5[i], var6[i],
					var7[i], var8[i]);
			String contentType = res.response.header("Content-Type");
			org.testng.Assert.assertEquals("text/html; charset=UTF-8", contentType);
			String serverType = res.response.header("Server");
			Assert.assertEquals("Apache", serverType);

		}

	}

}
