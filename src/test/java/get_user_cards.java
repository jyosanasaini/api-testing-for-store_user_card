
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class get_user_cards {

	public static Pair get(String url,String command,String key,String var1) throws NoSuchAlgorithmException
	{
		String s = key+"|"+command+"|"+var1+"|"+"1b1b0";
		
		String hash = helper.hashGenerator(s);
		RestAssured.baseURI=url;
		Response response=RestAssured.given().urlEncodingEnabled(true)
				.param("form","2")
				.param("key", key)
				.param("command",command)
				.param("hash", hash)
				.param("var1",var1)
				.post()
				.then()
				.statusCode(200)
				.extract()
				.response();
		
		JsonPath json = response.jsonPath();
		String token="";
		String msg = json.getString("msg");
		Pair pair = new Pair();
		if(msg.equals("Card not found."))
		{
		   	pair.token = "Token not found";
		   	pair.response = response;
		   	return pair;
		}
		
		Map<String, Map<String, String>> val = new LinkedHashMap<String, Map<String, String>>();
		val = json.get("user_cards");
		Map.Entry<String, Map<String, String>> entry = val.entrySet().iterator().next();
		token = entry.getKey();
        
		pair.response = response;
		pair.token = token;
		//System.out.println("get " + pair.token);
		return pair;			
	}
	
}
