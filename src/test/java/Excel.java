
import java.io.FileInputStream;

import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	public int getRowStart() throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		return rowStart;
	}

	public int getRowEnd() {
		int rowend = 0;
		try {
			ClassLoader classLoader = this.getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			rowend = sheet.getLastRowNum();
			file.close();

		} catch (Exception e) {
			System.out.println("Exception occur in getting row End");
		}
		return rowend;
	}

	public String getUrl() throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);

		Row row = sheet.getRow(1);
		String url = row.getCell(0).getStringCellValue();
		return url;
	}

	public String[] getCommand() throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);
		String command[] = new String[10];
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; ++i) {
			Row row = sheet.getRow(i);
			command[i] = row.getCell(1).getStringCellValue();
		}
		return command;

	}

	public String getKey() throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);

		Row row = sheet.getRow(1);
		String key = row.getCell(2).getStringCellValue();
		return key;
	}

	public String[] getVar(int index) throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);
		String var[] = new String[10];
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; ++i) {
			Row row = sheet.getRow(i);
			var[i] = row.getCell(index + 2).getStringCellValue();
		}
		return var;

	}

	public int[] getIntVar(int index) throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);
		int var[] = new int[10];
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; ++i) {
			Row row = sheet.getRow(i);
			var[i] = (int) row.getCell(index + 2).getNumericCellValue();
		}
		return var;
	}

	public int[] getStatus() throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelData1.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheetAt(0);
		int form[] = new int[10];
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; ++i) {
			Row row = sheet.getRow(i);
			form[i] = (int) row.getCell(11).getNumericCellValue();
		}
		return form;

	}

}