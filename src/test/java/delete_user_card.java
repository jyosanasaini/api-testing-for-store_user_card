
import java.security.NoSuchAlgorithmException;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class delete_user_card {

	public static Pair Delete_card(String url, String command, String key, String var1, String var2, String var3,
			String var4, String var5, String var6, int var7, int var8) throws NoSuchAlgorithmException {
		String input = "smsplus|" + command + "|" + var1 + "|1b1b0";

		String hash =  helper.hashGenerator(input);
		Pair del = get_user_cards.get( url,"get_user_cards",key,var1);
		String token = del.token;
		Pair pair = new Pair();
		 if(token.equals("Token not found"))
		 {
			 pair.token=token;
			 pair.response=del.response;
			 return pair;
		 }
		RestAssured.baseURI = url;
		Response response = (Response) RestAssured.given().param("form", "2").param("key", key)
				.param("command", command).param("hash", hash).param("var1", var1).param("var2", token).post().then()
				.statusCode(200).extract().response();
		
		pair.response = response;
		pair.token = "";
		return pair;

	}

}
